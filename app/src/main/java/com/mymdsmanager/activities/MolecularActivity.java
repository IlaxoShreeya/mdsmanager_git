package com.mymdsmanager.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.mymdsmanager.MyApplication.MyApplication;
import com.mymdsmanager.R;
import com.mymdsmanager.database.DBAdapter;
import com.mymdsmanager.datacontrollers.Constants;
import com.mymdsmanager.task.HomeWatcher;
import com.mymdsmanager.task.OnHomePressedListener;
import com.mymdsmanager.wrapper.LinkMessageWrapper;
import com.mymdsmanager.wrapper.Molecularwrapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by suarebits on 28/3/17.
 */
public class MolecularActivity extends AppCompatActivity implements OnFinishActivity {

    @Bind(R.id.saveBtn)
    Button saveBtn;
    @Bind(R.id.spinnerIdh2)
    Spinner spinnerIdh2;
    @Bind(R.id.spinnerIdh1)
    Spinner spinnerIdh1;
    @Bind(R.id.spinnerTet2)
    Spinner spinnerTet2;
    @Bind(R.id.spinnerTp53)
    Spinner spinnerTp53;
    @Bind(R.id.spinnerSf3b1)
    Spinner spinnerSf3b1;
    @Bind(R.id.spinnerSrsf2)
    Spinner spinnerSrsf2;
    @Bind(R.id.spinneJak2)
    Spinner spinneJak2;
    @Bind(R.id.spinnerAsxl1)
    Spinner spinnerAsxl1;
    @Bind(R.id.spinnerDnmt3a)
    Spinner spinnerDnmt3a;
    @Bind(R.id.spinnerEzh2)
    Spinner spinnerEzh2;
    @Bind(R.id.spinnerEtv6)
    Spinner spinnerEtv6;
    @Bind(R.id.spinnerRunx1)
    Spinner spinnerRunx1;
    @Bind(R.id.spinnerU2af1)
    Spinner spinnerU2af1;
    @Bind(R.id.spinnerZrsr2)
    Spinner spinnerZrsr2;
    @Bind(R.id.spinnerNraskras)
    Spinner spinnerNraskras;
    @Bind(R.id.spinnerCbl)
    Spinner spinnerCbl;

    Molecularwrapper molecularwrapper;
    private DBAdapter dbAdapter;
    Context context;
    HomeWatcher mHomeWatcher;
    boolean isActivityFound = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_molecular);
        ButterKnife.bind(this);
        context = MolecularActivity.this;
        dbAdapter = new DBAdapter(MolecularActivity.this);
        setToolBar();
        initUI();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    Constants.PERMISSION_READ_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.PERMISSION_WRITE_EXTERNAL_STORAGE);
        }

    }


    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
        MyApplication.setCustomToolBar(toolbar, MolecularActivity.this, getString(R.string.molecular_profile));
//        getSupportActionBar().setTitle(getString(R.string.molecular_profile));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initUI() {
        ArrayList<String> typeArray = new ArrayList<>();
        typeArray.add("Unknown");
        typeArray.add("Positive");
        typeArray.add("Negative");

        spinnerIdh2.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerIdh1.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerTet2.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerTp53.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerSf3b1.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerSrsf2.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinneJak2.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerAsxl1.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerDnmt3a.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerEzh2.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerEtv6.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerRunx1.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerU2af1.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerZrsr2.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerNraskras.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));
        spinnerCbl.setAdapter(new ArrayAdapter<String>(context, R.layout.molecular_item, R.id.textview, typeArray));


        molecularwrapper = new Molecularwrapper();
        dbAdapter.openMdsDB();
        molecularwrapper = dbAdapter.getMolecularDetails();
        dbAdapter.closeMdsDB();

        spinnerIdh2.setSelection(typeArray.indexOf(molecularwrapper.getIdh2Value()));
        spinnerIdh1.setSelection(typeArray.indexOf(molecularwrapper.getIdh1Value()));
        spinnerTet2.setSelection(typeArray.indexOf(molecularwrapper.getTet2Value()));
        spinnerTp53.setSelection(typeArray.indexOf(molecularwrapper.getTp53Value()));
        spinnerSf3b1.setSelection(typeArray.indexOf(molecularwrapper.getSf3b1Value()));
        spinnerSrsf2.setSelection(typeArray.indexOf(molecularwrapper.getSrsf2Value()));
        spinneJak2.setSelection(typeArray.indexOf(molecularwrapper.getJak2Value()));
        spinnerAsxl1.setSelection(typeArray.indexOf(molecularwrapper.getAsxl1Value()));
        spinnerDnmt3a.setSelection(typeArray.indexOf(molecularwrapper.getDnmt3aValue()));
        spinnerEzh2.setSelection(typeArray.indexOf(molecularwrapper.getEzh2Value()));
        spinnerEtv6.setSelection(typeArray.indexOf(molecularwrapper.getEtv6Value()));
        spinnerRunx1.setSelection(typeArray.indexOf(molecularwrapper.getRunx1Value()));
        spinnerU2af1.setSelection(typeArray.indexOf(molecularwrapper.getU2af1Value()));
        spinnerZrsr2.setSelection(typeArray.indexOf(molecularwrapper.getZrsr2Value()));
        spinnerNraskras.setSelection(typeArray.indexOf(molecularwrapper.getNraskrasValue()));
        spinnerCbl.setSelection(typeArray.indexOf(molecularwrapper.getCblValue()));


        mHomeWatcher = new HomeWatcher(MolecularActivity.this);
        mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                // do something here...
                isActivityFound = true;
            }

            @Override
            public void onHomeLongPressed() {
            }
        });
        mHomeWatcher.startWatch();

    }

    @Override
    protected void onResume() {
        super.onResume();
        new UpdateDataDialog(MolecularActivity.this, R.style.Dialog).dismiss();
        mHomeWatcher.startWatch();
        if (isActivityFound) {
            new UpdateDataDialog(MolecularActivity.this, R.style.Dialog);
            isActivityFound = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            mHomeWatcher.stopWatch();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @OnClick(R.id.saveBtn)
    public void onClick() {
        updateMolecular();
    }

    private void updateMolecular() {

        dbAdapter.openMdsDB();
        molecularwrapper.setIdh2Value(spinnerIdh2.getSelectedItem().toString());
        molecularwrapper.setIdh1Value(spinnerIdh1.getSelectedItem().toString());
        molecularwrapper.setTet2Value(spinnerTet2.getSelectedItem().toString());
        molecularwrapper.setTp53Value(spinnerTp53.getSelectedItem().toString());
        molecularwrapper.setSf3b1Value(spinnerSf3b1.getSelectedItem().toString());
        molecularwrapper.setSrsf2Value(spinnerSrsf2.getSelectedItem().toString());
        molecularwrapper.setJak2Value(spinneJak2.getSelectedItem().toString());
        molecularwrapper.setAsxl1Value(spinnerAsxl1.getSelectedItem().toString());
        molecularwrapper.setDnmt3aValue(spinnerDnmt3a.getSelectedItem().toString());
        molecularwrapper.setEzh2Value(spinnerEzh2.getSelectedItem().toString());
        molecularwrapper.setEtv6Value(spinnerEtv6.getSelectedItem().toString());
        molecularwrapper.setRunx1Value(spinnerRunx1.getSelectedItem().toString());
        molecularwrapper.setU2af1Value(spinnerU2af1.getSelectedItem().toString());
        molecularwrapper.setZrsr2Value(spinnerZrsr2.getSelectedItem().toString());
        molecularwrapper.setNraskrasValue(spinnerNraskras.getSelectedItem().toString());
        molecularwrapper.setCblValue(spinnerCbl.getSelectedItem().toString());
        dbAdapter.updateMolecularList(molecularwrapper);
        dbAdapter.closeMdsDB();


        if (MyApplication.getBooleanPrefs(Constants.IS_MOLECULAR_FIRST_TIME)) {
            MyApplication.saveLocalData(true);
            new UpdateOnClass(MyApplication.getApplication(), MolecularActivity.this);
            saveAlert();
        } else {
            linkDialog();
        }
    }

    private void saveAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("" + getResources().getString(R.string.app_name3));
        builder.setMessage("Saved Successfully")
                .setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                                finish();
                            }
                        });
        builder.show();
    }

    private void linkDialog() {
        addMessageToLinkMessageTable();
        MyApplication.saveBooleanPrefs(Constants.IS_MOLECULAR_FIRST_TIME, true);
        MyApplication.saveLocalData(true);
        new UpdateOnClass(MyApplication.getApplication(), MolecularActivity.this);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("" + getResources().getString(R.string.quicktip));
        builder.setMessage(getResources().getString(R.string.molecular_learn_more))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.learn_more),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                finish();

                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.MOLECULAR_URL)));
                            }
                        })
                .setNegativeButton(getResources().getString(R.string.not_now), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        builder.show();
    }

    private void addMessageToLinkMessageTable() {

        Date date = new Date();
        String modifiedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

        LinkMessageWrapper linkMessageWrapper = new LinkMessageWrapper();
        dbAdapter.openMdsDB();
        linkMessageWrapper.setMessage(getResources().getString(R.string.molecular_msg_text));
        linkMessageWrapper.setMessageUrl(Constants.MOLECULAR_URL);
        linkMessageWrapper.setMessageDate(modifiedDate);
        dbAdapter.saveLinkMessage(linkMessageWrapper);
        dbAdapter.closeMdsDB();
    }

    @Override
    public void onFinish() {

    }

    //permission work
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case Constants.PERMISSION_READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                break;
            case Constants.PERMISSION_WRITE_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                break;
        }
    }



}
