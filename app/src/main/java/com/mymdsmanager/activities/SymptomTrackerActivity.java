package com.mymdsmanager.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mymdsmanager.MyApplication.MyApplication;
import com.mymdsmanager.R;
import com.mymdsmanager.database.DBAdapter;
import com.mymdsmanager.datacontrollers.Constants;
import com.mymdsmanager.datacontrollers.DataManager;
import com.mymdsmanager.task.HomeWatcher;
import com.mymdsmanager.task.OnHomePressedListener;
import com.mymdsmanager.wrapper.LinkMessageWrapper;
import com.mymdsmanager.wrapper.SymptomDetailWrapper;
import com.mymdsmanager.wrapper.SymptomWrapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SymptomTrackerActivity extends AppCompatActivity implements OnFinishActivity {
    @Bind(R.id.symptomSpinner)
    Spinner symptomSpinner;
    @Bind(R.id.addSymptomBtn)
    ImageButton addSymptomBtn;
    @Bind(R.id.severitySeekBar)
    SeekBar severitySeekBar;
    @Bind(R.id.selectDateTxt)
    TextView selectDateTxt;
    @Bind(R.id.selectTimeTxt)
    TextView selectTimeTxt;
    @Bind(R.id.saveSymtomBtn)
    Button saveSymtomBtn;
    @Bind(R.id.viewHistoryText)
    TextView viewHistoryText;
    @Bind(R.id.enterDurationEdt)
    Button enterDurationEdt;
    @Bind(R.id.enterFrequencyEdt)
    Button enterFrequencyEdt;
    @Bind(R.id.subsymptomSpinner)
    Spinner subsymptomSpinner;
    @Bind(R.id.symptomSpinnertxt)
    TextView symptomSpinnertxt;
    @Bind(R.id.subsymptomSpinnertxt)
    TextView subsymptomSpinnertxt;
    private DatePicker datePicker;
    private Toolbar toolbar;
    private int year;
    private int month;
    private int day;
    private final String SELECT_DURATION = "SELECT DURATION";
    private final String SELECT_FREQUENCY = "SELECT FREQUENCY";
    private final int DATE_DIALOG_ID = 1;
    private final int TIME_DIALOG_ID = 2;
    private DBAdapter dbAdapter;
    @Bind(R.id.notesEdt)
    EditText notesEdt;
    private String durationString = "", frequencyString = "";
    private boolean isChartSelected = false;
    private int id = -1;
    ArrayList<String> symtomArrayList = new ArrayList<>();
    ArrayList<String> durationList = new ArrayList<>();
    ArrayList<String> frequencyList = new ArrayList<>();
    String subsymptom_str = "";
    ProgressDialog dialog;
    ArrayList<String> symptom_Arraylist = new ArrayList<>();
    ArrayList<String> practicalProblems_Arraylist = new ArrayList<>();
    SymptomDetailWrapper wrapper;
    HomeWatcher mHomeWatcher;

    String selected_url = "", selected_text = "", selected_msg_text = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  MyApplication.getApplication().hideSoftKeyBoard(SymptomTrackerActivity.this)
        setContentView(R.layout.activity_symptom_tracker);
        ButterKnife.bind(this);
        dbAdapter = new DBAdapter(SymptomTrackerActivity.this);
        if (wrapper == null) {
//            selectDateTxt.setText(formattedDate);
//            selectTimeTxt.setText(formattedtime);
            enterDurationEdt.setText("Enter Duration");
            enterFrequencyEdt.setText("Enter Frequency");
        }
        getUiComponents();
        mHomeWatcher = new HomeWatcher(SymptomTrackerActivity.this);
        mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                // do something here...
                isActivityFound = true;
            }

            @Override
            public void onHomeLongPressed() {
            }
        });
        mHomeWatcher.startWatch();


        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    Constants.PERMISSION_READ_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constants.PERMISSION_WRITE_EXTERNAL_STORAGE);
        }

    }

    boolean isActivityFound = false;
    boolean fristSeek = true;

    private void getUiComponents() {
        //Syspto items
        id = getIntent().getIntExtra("id", -1);
        symptom_Arraylist.add("Anxiety");
        symptom_Arraylist.add("Bleeding or Bruising");
        symptom_Arraylist.add("Changes in urination");
        symptom_Arraylist.add("Constipation");
        symptom_Arraylist.add("Depression");
        symptom_Arraylist.add("Diarrhea");
        symptom_Arraylist.add("Difficulty getting around");
        symptom_Arraylist.add("Difficulty sleeping");
        symptom_Arraylist.add("Fatigue");
        symptom_Arraylist.add("Fear");
        symptom_Arraylist.add("Fevers");
        symptom_Arraylist.add("Indigestion");
        symptom_Arraylist.add("Lack of Appetite");
        symptom_Arraylist.add("Loss of interest in usual activities");
        symptom_Arraylist.add("Memory  or concentration problems");
        symptom_Arraylist.add("Mouth sores");
        symptom_Arraylist.add("Nausea");
        symptom_Arraylist.add("Pain");
        symptom_Arraylist.add("Sadness");
        symptom_Arraylist.add("Sexual dysfunction");
        symptom_Arraylist.add("Shortness of breath");
        symptom_Arraylist.add("Skin changes (dry, itching, rash)");
        symptom_Arraylist.add("Vomiting");
        symptom_Arraylist.add("Worry");

        //pratical problem
        practicalProblems_Arraylist.add("Caregiving");
        practicalProblems_Arraylist.add("Finances");
        practicalProblems_Arraylist.add("Housing");
        practicalProblems_Arraylist.add("Insurance");
        practicalProblems_Arraylist.add("Spiritual/religious concerns");
        practicalProblems_Arraylist.add("Transportation");
        practicalProblems_Arraylist.add("Work-related problems");


        dbAdapter.openMdsDB();
        ArrayList<SymptomWrapper> otherSympomList = dbAdapter.getOtherSymptomList();
        if (otherSympomList.size() > 0) {
            for (int i = 0; i < otherSympomList.size(); i++) {
                if (otherSympomList.get(i).getSymptom().equalsIgnoreCase("Symptoms")) {
                    symptom_Arraylist.add(otherSympomList.get(i).getDescription());
                } else {
                    practicalProblems_Arraylist.add(otherSympomList.get(i).getDescription());
                }
            }
        }

        dbAdapter.closeMdsDB();


        DataManager.getInstance().setSymtomsubArrayList(symptom_Arraylist);
        DataManager.getInstance().setPraticalsubArraylist(practicalProblems_Arraylist);


        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        MyApplication.setCustomToolBar(toolbar, SymptomTrackerActivity.this, "Symptom Tracker");
//        getSupportActionBar().setTitle("Symptom Tracker");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setSymptomSpinner();
            }
        });

        durationList.add("Minutes");
        durationList.add("Hours");
        durationList.add("Days");
        durationList.add("Weeks");
        durationList.add("Months");
        durationList.add("Years");
        frequencyList.add("per minute");
        frequencyList.add("per Hour");
        frequencyList.add("per day");
        frequencyList.add("per week");
        frequencyList.add("per month");
        frequencyList.add("per year");
        severitySeekBar.incrementProgressBy(10);
        severitySeekBar.setMax(90);
        severitySeekBar.setProgress(40);

        severitySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress / 10;
                progress = progress * 10;
                seekBar.setProgress(progress);
                if (!fristSeek) {
                    dataChanged = true;
                }
                fristSeek = false;
                System.out.println("progress" + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        if (id > 0) {
            viewHistoryText.setVisibility(View.GONE);
            setData();

        }
        new CountDownTimer(1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                notesEdt.addTextChangedListener(textWatcher);
                enterDurationEdt.addTextChangedListener(textWatcher);
                enterFrequencyEdt.addTextChangedListener(textWatcher);
                selectDateTxt.addTextChangedListener(textWatcher);
                selectTimeTxt.addTextChangedListener(textWatcher);
            }
        }.start();

    }

    private void showPopUp(final String title, final ArrayList<String> arr) {


        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);

        ListView modeList = new ListView(this);
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, arr);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();

        dialog.show();

        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();


                if (title.equalsIgnoreCase(SELECT_DURATION)) {

                    if (TextUtils.isEmpty(durationString)) {
                        durationString = arr.get(position).replaceAll("feet", "");
                        ArrayList<String> list = new ArrayList<>();
                        for (int i = 1; i <= 60; i++) {
                            list.add(i + "");
                        }
                        showPopUp(SELECT_DURATION, list);
                    } else {
                        durationString = arr.get(position) + " " + durationString;
                        enterDurationEdt.setText(durationString);

                    }

                } else if (title.equalsIgnoreCase(SELECT_FREQUENCY)) {

                    if (TextUtils.isEmpty(frequencyString)) {
                        frequencyString = arr.get(position).replaceAll("feet", "");
                        ArrayList<String> list = new ArrayList<>();
                        for (int i = 1; i <= 49; i++) {
                            list.add(i + "");
                        }

                        showPopUp(SELECT_FREQUENCY, list);
                    } else {
                        frequencyString = arr.get(position) + " " + frequencyString;
                        enterFrequencyEdt.setText(frequencyString);

                    }
                }
            }
        });


    }

    private void setData() {
        try {
            if (id > 0) {
                dbAdapter.openMdsDB();
                wrapper = dbAdapter.getSymtomWrapper(id);
                viewHistoryText.setVisibility(View.GONE);
                saveSymtomBtn.setText("Save Symptom");
                notesEdt.setText(wrapper.getNotes());
                symptomSpinner.setSelection(symtomArrayList.indexOf(wrapper.getSymptomname()));
//                subsymptomSpinner.setSelection();
                enterFrequencyEdt.setText(wrapper.getFrequency());
                enterDurationEdt.setText(wrapper.getDuration());
                selectTimeTxt.setText(wrapper.getSymptomtime());
                selectDateTxt.setText(wrapper.getSymptomdate());
                if (!TextUtils.isEmpty(wrapper.getSeverity())) {
                    severitySeekBar.setProgress((Integer.parseInt(wrapper.getSeverity()) * 10 - 10));
                }

                dbAdapter.closeMdsDB();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.addSymptomBtn)
    public void addSymptomBtn() {
        showAddSymptomDialog();
    }

    @OnClick(R.id.enterDurationEdt)
    public void enterDurationEdt() {
        durationString = "";

        showPopUp(SELECT_DURATION, durationList);

    }

    @OnClick(R.id.enterFrequencyEdt)
    public void enterFrequencyEdt() {
        frequencyString = "";

        showPopUp(SELECT_FREQUENCY, frequencyList);
    }

    boolean showToast = true;

    @Override
    protected void onResume() {
        super.onResume();
        new UpdateDataDialog(SymptomTrackerActivity.this, R.style.Dialog).dismiss();
        mHomeWatcher.startWatch();
        if (isActivityFound) {
            new UpdateDataDialog(SymptomTrackerActivity.this, R.style.Dialog);
            isActivityFound = false;
        }
        Calendar c = Calendar.getInstance();


        SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy");
        SimpleDateFormat time_df = new SimpleDateFormat("KK:mm");
        String formattedDate = df.format(c.getTime());
        String formattedtime = time_df.format(c.getTime());


    }

    ArrayList<String> sub_praticalStrings;

    private void setSymptomSpinner() {

        symtomArrayList.add("Select Symptom Category *");
        symtomArrayList.add("Symptoms");
        symtomArrayList.add("Practical Problems");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, symtomArrayList);
        symptomSpinner.setAdapter(adapter);

        symptomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                sub_praticalStrings = new ArrayList<String>();
                sub_praticalStrings.add("Select Symptom Sub Category *");
                if (position == 1) {
                    sub_praticalStrings.addAll(DataManager.getInstance().getSymtomsubArrayList());
                    sub_praticalStrings.add("Other");

                    ArrayAdapter<String> sub_symptom_adapter = new ArrayAdapter<String>(SymptomTrackerActivity.this, android.R.layout.simple_dropdown_item_1line, sub_praticalStrings);
                    subsymptomSpinner.setAdapter(sub_symptom_adapter);
                    if (wrapper != null) {

                        subsymptomSpinner.setSelection(sub_praticalStrings.indexOf(wrapper.getSubsymptom_str()));
                    }
                } else if (position == 2) {

                    sub_praticalStrings.addAll(DataManager.getInstance().getPraticalsubArraylist());
                    sub_praticalStrings.add("Other");
                    ArrayAdapter<String> sub_pratical_adapter = new ArrayAdapter<String>(SymptomTrackerActivity.this, android.R.layout.simple_dropdown_item_1line, sub_praticalStrings);
                    subsymptomSpinner.setAdapter(sub_pratical_adapter);
                    if (wrapper != null) {
                        subsymptomSpinner.setSelection(sub_praticalStrings.indexOf(wrapper.getSubsymptom_str()));
                    }
                } else if (position == 0) {
                    ArrayAdapter<String> sub_symptom_adapter = new ArrayAdapter<String>(SymptomTrackerActivity.this, android.R.layout.simple_dropdown_item_1line, sub_praticalStrings);
                    subsymptomSpinner.setAdapter(sub_symptom_adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subsymptomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
                if (position == sub_praticalStrings.size() - 1 && subsymptomSpinner.getSelectedItem().toString().equalsIgnoreCase("Other")) {
                    showAddOthersDialog(subsymptomSpinner, "type");
                } else {
                    subsymptom_str = subsymptomSpinner.getSelectedItem().toString();
                    getURlAndText(subsymptom_str, symptomSpinner.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        subsymptomSpinner.setOnTouchListener(spinnerOnTouch);
//        hideKeyboard();

    }

    private View.OnTouchListener spinnerOnTouch = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                //Your code
                if (symptomSpinner.getSelectedItem().toString().equalsIgnoreCase("Select Symptom Category *")) {
                    Toast.makeText(SymptomTrackerActivity.this, "Please select symptom category before the selection of sub category", Toast.LENGTH_SHORT).show();

                }

            }
            return false;
        }
    };

    @OnClick(R.id.saveSymtomBtn)


    public void saveSymtomBtn() {
        if (TextUtils.isEmpty(selectDateTxt.getText().toString()) || symptomSpinner.getSelectedItem().toString().equalsIgnoreCase("Select Symptom Category *") || subsymptom_str.equalsIgnoreCase("Select Symptom Sub Category *")) {
            Toast.makeText(SymptomTrackerActivity.this, "Please fill required fields", Toast.LENGTH_LONG).show();
        } else {
            dbAdapter.openMdsDB();

            SymptomDetailWrapper wrapper = new SymptomDetailWrapper();
            wrapper.setSymptomname(symptomSpinner.getSelectedItem().toString());
            wrapper.setSymptomtime(selectTimeTxt.getText().toString());
            wrapper.setSymptomdate(selectDateTxt.getText().toString());
            wrapper.setDate_to_order((double) getMillisTime(selectDateTxt.getText().toString(), "MMM dd, yyyy"));
            int severity = severitySeekBar.getProgress();
            severity = severity + 10;
            severity = severity / 10;
            wrapper.setSeverity("" + severity);
            wrapper.setNotes(notesEdt.getText().toString());
            wrapper.setFrequency(enterFrequencyEdt.getText().toString());
            wrapper.setDuration(enterDurationEdt.getText().toString());
            wrapper.setSubsymptom_str(subsymptom_str);
            if (id == -1) {
                dbAdapter.saveSymptomDetail(wrapper);
                dbAdapter.closeMdsDB();
                MyApplication.saveLocalData(true);


                if (!MyApplication.getBooleanPrefs(Constants.IS_SYMPTON_TRACKER_FIRST_TIME)) {
                    linkDialog(selected_url, selected_text);
                } else {
                    if (severity >= 6) {
                        linkDialog(selected_url, selected_text);
                    } else {
                        new UpdateOnClass(MyApplication.getApplication(), this);
                        Toast.makeText(SymptomTrackerActivity.this, "Symptom Added", Toast.LENGTH_SHORT).show();
                        clearData();
                        startActivity(new Intent(SymptomTrackerActivity.this, SymptomListGraphActivity.class));
                    }
                }
            } else {

                dbAdapter.updateSymtomData(wrapper, id);
                dbAdapter.closeMdsDB();
                MyApplication.saveLocalData(true);
                new UpdateOnClass(MyApplication.getApplication(), this);
                Toast.makeText(SymptomTrackerActivity.this, "Symptom Updated", Toast.LENGTH_SHORT).show();
                clearData();
                startActivity(new Intent(SymptomTrackerActivity.this, SymptomListGraphActivity.class));
            }
        }
    }

    private void clearData() {
        dataChanged = false;
        notesEdt.setText("");
        enterDurationEdt.setText("");
        enterFrequencyEdt.setText("");
        selectTimeTxt.setText("");
        selectDateTxt.setText("");
        symptomSpinner.setSelection(0);
        subsymptomSpinner.setSelection(0);
    }


    private void showAddSymptomDialog() {
        final Dialog dialog = new Dialog(SymptomTrackerActivity.this, R.style.AppCompatTheme);
        // Include dialog.xml file
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_add_symptom);
// Check if no view has focus:

        final EditText symptomEdt = (EditText) dialog.findViewById(R.id.symptomEdt);


        symptomEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        if (TextUtils.isEmpty(symptomEdt.getText().toString())) {
                            Toast.makeText(SymptomTrackerActivity.this, "Please fill required fields", Toast.LENGTH_LONG).show();
                        } else {
                            dbAdapter.openMdsDB();

                            SymptomWrapper wrapper = new SymptomWrapper();
                            String text = symptomEdt.getText().toString();
                            wrapper.setSymptom(text);

                            dbAdapter.saveSymptom(wrapper);

                            dbAdapter.closeMdsDB();
                            MyApplication.saveLocalData(true);
                            new UpdateOnClass(MyApplication.getApplication(), SymptomTrackerActivity.this);
                            dialog.dismiss();
                            Toast.makeText(SymptomTrackerActivity.this, "New Symptom Added", Toast.LENGTH_LONG).show();
                            setSymptomSpinner();
                            hideKeyboard();
                        }
                }
                return true;

            }
        });

        Button addSymptomBtn, closeBtn;
        addSymptomBtn = (Button) dialog.findViewById(R.id.addSymptomBtn);
        closeBtn = (Button) dialog.findViewById(R.id.closeBtn);

        addSymptomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(symptomEdt.getText().toString())) {
                    Toast.makeText(SymptomTrackerActivity.this, "Please fill required fields", Toast.LENGTH_LONG).show();
                } else {
                    dbAdapter.openMdsDB();

                    SymptomWrapper wrapper = new SymptomWrapper();
                    String text = symptomEdt.getText().toString();
                    wrapper.setSymptom(text);
                    dbAdapter.saveSymptom(wrapper);
                    dbAdapter.closeMdsDB();
                    MyApplication.saveLocalData(true);
                    new UpdateOnClass(MyApplication.getApplication(), SymptomTrackerActivity.this);
                    dialog.dismiss();
                    Toast.makeText(SymptomTrackerActivity.this, "New Symptom Added", Toast.LENGTH_LONG).show();

                    setSymptomSpinner();
// Check if no view has focus:
                    hideKeyboard();
                }
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();

    }

    // Check if no view has focus:
    private void hideKeyboard() {
        // Check if no view has focus:

        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//        if (imm.isActive()) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0); // hide
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.selectDateTxt)
    public void setSelectDateTxt() {
        showDialog(DATE_DIALOG_ID);

    }

    @OnClick(R.id.viewHistoryText)
    public void viewHistoryText() {
        mHomeWatcher.stopWatch();
        startActivity(new Intent(SymptomTrackerActivity.this, SymptomListGraphActivity.class));
    }

    @OnClick(R.id.selectTimeTxt)
    public void selectTimeTxt() {
        showDialog(TIME_DIALOG_ID);

    }


    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar c = Calendar.getInstance();

        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date

                return new DatePickerDialog(this, datePickerListener,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

            case TIME_DIALOG_ID:

                return new TimePickerDialog(this,
                        timePickerListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            String dayString = day + "", monthString = (month + 1) + "";

            if ((month + 1) < 10) {
                monthString = "0" + (month + 1);
            }
            if (day < 10) {
                dayString = "0" + day;
            }
            String dateString = monthString + "-" + dayString + "-" + year;
            // set selected date into textview
            selectDateTxt.setText(formateDate(dateString));
        }
    };
    private TimePickerDialog.OnTimeSetListener timePickerListener
            = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            selectTimeTxt.setText(new StringBuilder().append(pad(hourOfDay))
                    .append(":").append(pad(minute)));
        }
    };

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    private String formateDate(String dateString) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat("MMM dd, yyyy");
            Date date = originalFormat.parse(dateString);
            String formattedDate = targetFormat.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onBackPressed() {

        if (dataChanged || (!symptomSpinner.getSelectedItem().toString().equalsIgnoreCase("Select Symptom Category *")
                || !subsymptom_str.equalsIgnoreCase("Select Symptom Sub Category *"))
                ) {
            saveAlert();
        } else {

            finish();
        }


    }

    boolean dataChanged = false;
    private TextWatcher textWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {
            dataChanged = true;

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            dataChanged = true;
        }
    };

    private void saveAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SymptomTrackerActivity.this);
        builder.setTitle("" + getResources().getString(R.string.app_name3));
        builder.setMessage("You haven't saved data yet. Do you really want to cancel the process?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                finish();


                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    @Override
    public void onFinish() {


    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            mHomeWatcher.stopWatch();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void showAddOthersDialog(final Spinner typeSpinner, final String title) {

        final Dialog dialog = new Dialog(SymptomTrackerActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        // Include dialog.xml file
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_add_others);
// Check if no view has focus:
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final EditText enterValueEdt = (EditText) dialog.findViewById(R.id.enterValueEdt);
        LinearLayout customListlayout = (LinearLayout) dialog.findViewById(R.id.customListlayout);
        LinearLayout otherListLayout = (LinearLayout) dialog.findViewById(R.id.otherListLayout);


        dbAdapter.openMdsDB();
        ArrayList<SymptomWrapper> otherSympomList = dbAdapter.getOtherSymptomList();
        if (otherSympomList.size() > 0) {
            for (int i = 0; i < otherSympomList.size(); i++) {
                if (symptomSpinner.getSelectedItem().toString().equalsIgnoreCase(otherSympomList.get(i).getSymptom())) {
                    View view = inflater.inflate(R.layout.list_other_symptom, null);
                    otherListLayout.addView(view);
                    TextView txtItem = (TextView) view.findViewById(R.id.txt_item);
                    txtItem.setText(otherSympomList.get(i).getDescription());
                }
            }
        } else {
            customListlayout.setVisibility(View.GONE);
        }

        dbAdapter.closeMdsDB();


        enterValueEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        if (TextUtils.isEmpty(enterValueEdt.getText().toString())) {
                            Toast.makeText(SymptomTrackerActivity.this, "Please fill required field", Toast.LENGTH_LONG).show();
                        } else {

                            if (title.equalsIgnoreCase("type")) {
                                sub_praticalStrings.add(sub_praticalStrings.size() - 1, enterValueEdt.getText().toString());


                                dbAdapter.openMdsDB();
                                SymptomWrapper wrapper = new SymptomWrapper();
                                wrapper.setSymptom(symptomSpinner.getSelectedItem().toString());
                                wrapper.setDescription(enterValueEdt.getText().toString());
                                dbAdapter.saveSymptom(wrapper);
                                dbAdapter.closeMdsDB();


                                if (symptomSpinner.getSelectedItem().toString().equalsIgnoreCase("Symptoms")) {
                                    ArrayList<String> tempList = DataManager.getInstance().getSymtomsubArrayList();
                                    tempList.add(enterValueEdt.getText().toString());
                                    DataManager.getInstance().setSymtomsubArrayList(tempList);

                                } else {
                                    ArrayList<String> tempList = DataManager.getInstance().getPraticalsubArraylist();
                                    tempList.add(enterValueEdt.getText().toString());
                                    DataManager.getInstance().setSymtomsubArrayList(tempList);
                                }
                                MyApplication.saveLocalData(true);
                                new UpdateOnClass(MyApplication.getApplication(), SymptomTrackerActivity.this);


                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SymptomTrackerActivity.this, android.R.layout.simple_spinner_item, sub_praticalStrings);
                                typeSpinner.setAdapter(adapter);
                                typeSpinner.setSelection(sub_praticalStrings.size() - 2);

                                dialog.dismiss();

                                hideKeyboard();
                            }
                        }
                }
                return true;

            }
        });

        Button addBtn, closeBtn;
        addBtn = (Button) dialog.findViewById(R.id.addBtn);
        closeBtn = (Button) dialog.findViewById(R.id.closeBtn);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(enterValueEdt.getText().toString())) {
                    Toast.makeText(SymptomTrackerActivity.this, "Please fill required field", Toast.LENGTH_LONG).show();
                } else {

                    if (title.equalsIgnoreCase("type")) {
                        sub_praticalStrings.add(sub_praticalStrings.size() - 1, enterValueEdt.getText().toString());
//                            typeArr.add("Others");

                        dbAdapter.openMdsDB();
                        SymptomWrapper wrapper = new SymptomWrapper();
                        wrapper.setSymptom(symptomSpinner.getSelectedItem().toString());
                        wrapper.setDescription(enterValueEdt.getText().toString());
                        dbAdapter.saveSymptom(wrapper);
                        dbAdapter.closeMdsDB();


                        if (symptomSpinner.getSelectedItem().toString().equalsIgnoreCase("Symptoms")) {
                            ArrayList<String> tempList = DataManager.getInstance().getSymtomsubArrayList();
                            tempList.add(enterValueEdt.getText().toString());
                            DataManager.getInstance().setSymtomsubArrayList(tempList);

                        } else {
                            ArrayList<String> tempList = DataManager.getInstance().getPraticalsubArraylist();
                            tempList.add(enterValueEdt.getText().toString());
                            DataManager.getInstance().setSymtomsubArrayList(tempList);
                        }
                        MyApplication.saveLocalData(true);
                        new UpdateOnClass(MyApplication.getApplication(), SymptomTrackerActivity.this);

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SymptomTrackerActivity.this, android.R.layout.simple_spinner_item, sub_praticalStrings);
                        typeSpinner.setAdapter(adapter);
                        typeSpinner.setSelection(sub_praticalStrings.size() - 2);

                        dialog.dismiss();

                        hideKeyboard();
                    }
                }
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        try {
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void linkDialog(final String url, String learnMoreText) {
        MyApplication.saveBooleanPrefs(Constants.IS_SYMPTON_TRACKER_FIRST_TIME, true);
        addMessageToLinkMessageTable(selected_url, selected_msg_text);
        new UpdateOnClass(MyApplication.getApplication(), this);
        AlertDialog.Builder builder = new AlertDialog.Builder(SymptomTrackerActivity.this);
        builder.setTitle("" + getResources().getString(R.string.quicktip));
        builder.setMessage(learnMoreText)
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.learn_more),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                clearData();
                                startActivity(new Intent(SymptomTrackerActivity.this, SymptomListGraphActivity.class));
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

                            }
                        })
                .setNegativeButton(getResources().getString(R.string.not_now), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        clearData();
                        startActivity(new Intent(SymptomTrackerActivity.this, SymptomListGraphActivity.class));

                    }
                });
        builder.show();
    }


    private void addMessageToLinkMessageTable(String url, String learnMoreText) {
        Date date = new Date();
        String modifiedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

        LinkMessageWrapper linkMessageWrapper = new LinkMessageWrapper();
        dbAdapter.openMdsDB();
        linkMessageWrapper.setMessage(learnMoreText);
        linkMessageWrapper.setMessageUrl(url);
        linkMessageWrapper.setMessageDate(modifiedDate);
        dbAdapter.saveLinkMessage(linkMessageWrapper);
        dbAdapter.closeMdsDB();
    }

    private void getURlAndText(String text, String type) {

        if (type.equalsIgnoreCase("Symptoms")) {

            if (text.equalsIgnoreCase("Anxiety")) {

                selected_text = getResources().getString(R.string.anxiety_text);
                selected_url = getResources().getString(R.string.anixiety_url);
                selected_msg_text = getResources().getString(R.string.anxiety_msg_text);

            } else if (text.equalsIgnoreCase("Bleeding or Bruising")) {


                selected_text = getResources().getString(R.string.bleding_text);
                selected_url = getResources().getString(R.string.bleding_url);
                selected_msg_text = getResources().getString(R.string.bleding_msg_text);
            } else if (text.equalsIgnoreCase("Changes in urination")) {

                selected_text = getResources().getString(R.string.urinition_text);
                selected_url = getResources().getString(R.string.urinition_url);
                selected_msg_text = getResources().getString(R.string.urinition_msg_text);

            } else if (text.equalsIgnoreCase("Constipation")) {

                selected_text = getResources().getString(R.string.constipation_text);
                selected_url = getResources().getString(R.string.constipation_url);
                selected_msg_text = getResources().getString(R.string.constipation_msg_text);

            } else if (text.equalsIgnoreCase("Depression")) {

                selected_text = getResources().getString(R.string.depression_text);
                selected_url = getResources().getString(R.string.depression_url);
                selected_msg_text = getResources().getString(R.string.depression_msg_text);

            } else if (text.equalsIgnoreCase("Diarrhea")) {

                selected_text = getResources().getString(R.string.diarrhea_text);
                selected_url = getResources().getString(R.string.diarrhea_url);
                selected_msg_text = getResources().getString(R.string.diarrhea_msg_text);
            } else if (text.equalsIgnoreCase("Difficulty getting around")) {

                selected_text = getResources().getString(R.string.getting_around_text);
                selected_url = getResources().getString(R.string.getting_around_url);
                selected_msg_text = getResources().getString(R.string.getting_msg_around_text);

            } else if (text.equalsIgnoreCase("Difficulty sleeping")) {

                selected_text = getResources().getString(R.string.sleeping_text);
                selected_url = getResources().getString(R.string.sleeping_url);
                selected_msg_text = getResources().getString(R.string.sleeping_msg_text);

            } else if (text.equalsIgnoreCase("Fatigue")) {/////////////////////////


                selected_text = getResources().getString(R.string.fatigue_text);
                selected_url = getResources().getString(R.string.fatigue_url);
                selected_msg_text = getResources().getString(R.string.fatigue_msg_text);

            } else if (text.equalsIgnoreCase("Fear")) {
                selected_text = getResources().getString(R.string.fear_text);
                selected_url = getResources().getString(R.string.fear_url);
                selected_msg_text = getResources().getString(R.string.fear_msg_text);

            } else if (text.equalsIgnoreCase("Fevers")) {

                selected_text = getResources().getString(R.string.fever_text);
                selected_url = getResources().getString(R.string.fever_url);
                selected_msg_text = getResources().getString(R.string.fever_msg_text);

            } else if (text.equalsIgnoreCase("Lack of Appetite")) {

                selected_text = getResources().getString(R.string.appetite_text);
                selected_url = getResources().getString(R.string.appetite_url);
                selected_msg_text = getResources().getString(R.string.appetite_msg_text);

            } else if (text.equalsIgnoreCase("Loss of interest in usual activities")) {

                selected_text = getResources().getString(R.string.loss_interest_text);
                selected_url = getResources().getString(R.string.loss_interest_url);
                selected_msg_text = getResources().getString(R.string.loss_interest_msg_text);

            } else if (text.equalsIgnoreCase("Memory  or concentration problems")) {

                selected_text = getResources().getString(R.string.memory_text);
                selected_url = getResources().getString(R.string.memory_url);
                selected_msg_text = getResources().getString(R.string.memory_msg_text);

            } else if (text.equalsIgnoreCase("Mouth sores")) {
                selected_text = getResources().getString(R.string.mouth_text);
                selected_url = getResources().getString(R.string.mouth_url);
                selected_msg_text = getResources().getString(R.string.mouth_msg_text);

            } else if (text.equalsIgnoreCase("Nausea")) {
                selected_text = getResources().getString(R.string.nausea_text);
                selected_url = getResources().getString(R.string.nausea_url);
                selected_msg_text = getResources().getString(R.string.nausea_msg_text);

            } else if (text.equalsIgnoreCase("Pain")) {
                selected_text = getResources().getString(R.string.pain_text);
                selected_url = getResources().getString(R.string.pain_url);
                selected_msg_text = getResources().getString(R.string.pain_msg_text);

            } else if (text.equalsIgnoreCase("Sadness")) {
                selected_text = getResources().getString(R.string.sadness_text);
                selected_url = getResources().getString(R.string.sadness_url);
                selected_msg_text = getResources().getString(R.string.sadness_msg_text);

            } else if (text.equalsIgnoreCase("Sexual dysfunction")) {
                selected_text = getResources().getString(R.string.sexual_text);
                selected_url = getResources().getString(R.string.sexual_url);
                selected_msg_text = getResources().getString(R.string.sexual_msg_text);

            } else if (text.equalsIgnoreCase("Shortness of breath")) {
                selected_text = getResources().getString(R.string.shortness_breath_text);
                selected_url = getResources().getString(R.string.shortness_breath_url);
                selected_msg_text = getResources().getString(R.string.shortness_breath_msg_text);

            } else if (text.equalsIgnoreCase("Skin changes (dry, itching, rash)")) {
                selected_text = getResources().getString(R.string.skin_changes_text);
                selected_url = getResources().getString(R.string.skin_changes_url);
                selected_msg_text = getResources().getString(R.string.skin_changes_msg_text);

            } else if (text.equalsIgnoreCase("Vomiting")) {
                selected_text = getResources().getString(R.string.vomiting_text);
                selected_url = getResources().getString(R.string.vomiting_url);
                selected_msg_text = getResources().getString(R.string.vomiting_msg_text);

            } else if (text.equalsIgnoreCase("Worry")) {
                selected_text = getResources().getString(R.string.worry_text);
                selected_url = getResources().getString(R.string.worry_url);
                selected_msg_text = getResources().getString(R.string.worry_msg_text);

            } else {
                selected_text = getResources().getString(R.string.symptoms_other_text);
                selected_url = getResources().getString(R.string.symptoms_other_url);
                selected_msg_text = getResources().getString(R.string.symptoms_other_msg_text);
            }


        } else {

            if (text.equalsIgnoreCase("Caregiving")) {
                selected_text = getResources().getString(R.string.child_care_text);
                selected_url = getResources().getString(R.string.child_care_url);
                selected_msg_text = getResources().getString(R.string.child_care_msg_text);

            } else if (text.equalsIgnoreCase("Finances")) {
                selected_text = getResources().getString(R.string.finance_text);
                selected_url = getResources().getString(R.string.finance_url);
                selected_msg_text = getResources().getString(R.string.finance_msg_text);

            } else if (text.equalsIgnoreCase("Insurance")) {
                selected_text = getResources().getString(R.string.insurance_text);
                selected_url = getResources().getString(R.string.insurance_url);
                selected_msg_text = getResources().getString(R.string.insurance_msg_text);

            } else if (text.equalsIgnoreCase("Spiritual/religious concerns")) {
                selected_text = getResources().getString(R.string.spritiual_text);
                selected_url = getResources().getString(R.string.spritiual_url);
                selected_msg_text = getResources().getString(R.string.spritiual_msg_text);

            } else if (text.equalsIgnoreCase("Transportation")) {
                selected_text = getResources().getString(R.string.transpotation_text);
                selected_url = getResources().getString(R.string.transpotation_url);
                selected_msg_text = getResources().getString(R.string.transpotation_msg_text);

            } else if (text.equalsIgnoreCase("Work-related problems")) {
                selected_text = getResources().getString(R.string.work_related_text);
                selected_url = getResources().getString(R.string.work_related_url);
                selected_msg_text = getResources().getString(R.string.work_related_msg_text);

            } else if (text.equalsIgnoreCase("Housing")) {
                selected_text = getResources().getString(R.string.housing_text);
                selected_url = getResources().getString(R.string.housing_url);
                selected_msg_text = getResources().getString(R.string.housing_msg_text);
            } else {
                selected_text = getResources().getString(R.string.practival_other_text);
                selected_url = getResources().getString(R.string.practival_other_url);
                selected_msg_text = getResources().getString(R.string.practival_other_msg_text);
            }
        }
    }


    public long getMillisTime(String time, String inputPattern) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        try {
            Date date = inputFormat.parse(time);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    //permission work
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case Constants.PERMISSION_READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                break;
            case Constants.PERMISSION_WRITE_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                break;
        }
    }

}
