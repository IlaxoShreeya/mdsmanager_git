package com.mymdsmanager.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mymdsmanager.MyApplication.MyApplication;
import com.mymdsmanager.R;
import com.mymdsmanager.activities.OnFinishActivity;
import com.mymdsmanager.activities.UpdateOnClass;
import com.mymdsmanager.database.DBAdapter;
import com.mymdsmanager.task.CompleteListener;
import com.mymdsmanager.task.GetDataTask;
import com.mymdsmanager.wrapper.LinkMessageWrapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nitin on 20/7/15.
 */
public class MessageFragment extends AppCompatActivity implements OnFinishActivity {


    @Bind(R.id.messageLayout)
    ListView messageLayout;
    @Bind(R.id.txtNoRecords)
    TextView txtNoRecords;
    private Toolbar toolbar;
    private DBAdapter dbAdapter;
    Context context;
    ArrayList<LinkMessageWrapper> messageList;
    MessageAdapter messageAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_messages);
        ButterKnife.bind(this);
        context = MessageFragment.this;
        dbAdapter = new DBAdapter(MessageFragment.this);
        setToolbar();
        messageList = new ArrayList<>();
        getNotifMessages();
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.icn_back);
        MyApplication.setCustomToolBar(toolbar, MessageFragment.this, "Messages");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getMessageData() {
        dbAdapter.openMdsDB();
        ArrayList<LinkMessageWrapper> messageDbList = dbAdapter.getLinkMessages();
        dbAdapter.closeMdsDB();
        messageList.addAll(messageDbList);


        Collections.sort(messageList, new Comparator<LinkMessageWrapper>() {
            public int compare(LinkMessageWrapper o1, LinkMessageWrapper o2) {
                return o1.getDate_to_order() < (o2.getDate_to_order()) ? -1 : 1;
            }
        });
        Collections.reverse(messageList);


        if (messageList.size() > 0) {
            //  Collections.reverse(messageList);
            messageAdapter = new MessageAdapter(MessageFragment.this, messageList);
            messageLayout.setAdapter(messageAdapter);
            txtNoRecords.setVisibility(View.GONE);
            messageLayout.setVisibility(View.VISIBLE);

            messageLayout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    if (!messageList.get(i).getMessageUrl().equalsIgnoreCase(""))
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(messageList.get(i).getMessageUrl())));
                }
            });


            messageLayout.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i, long l) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Are you sure you want to delete this Message?")
                            .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dbAdapter.openMdsDB();
                            dbAdapter.deleteMessageData(messageList.get(i).getMessageId() + "");
                            messageList.remove(i);
                            messageAdapter.refreshMe(messageList);
                            new UpdateOnClass(MyApplication.getApplication(), MessageFragment.this);
                            dbAdapter.closeMdsDB();

                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                    return false;
                }
            });


        } else {
            txtNoRecords.setVisibility(View.VISIBLE);
            messageLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFinish() {

    }


    private class MessageAdapter extends BaseAdapter {
        Context context;
        ArrayList<LinkMessageWrapper> rowItems;

        public MessageAdapter(Context context,
                              ArrayList<LinkMessageWrapper> arrayList) {
            this.context = context;
            this.rowItems = arrayList;
        }


        public void refreshMe(ArrayList<LinkMessageWrapper> arrayList) {
            this.rowItems = arrayList;
            notifyDataSetChanged();

        }

        /* private view holder class */
        private class ViewHolder {
            TextView timeTxt, messageTxt;

        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.custom_message_item,
                        null);
                holder = new ViewHolder();
                holder.timeTxt = (TextView) convertView
                        .findViewById(R.id.timeTxt);
                holder.messageTxt = (TextView) convertView
                        .findViewById(R.id.messageTxt);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.timeTxt.setText(rowItems.get(position).getMessageDate());
            holder.messageTxt.setText(rowItems.get(position).getMessage());


            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(position);

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    public void getNotifMessages() {
        try {

            if (!MyApplication.isConnectingToInternet(MessageFragment.this)) {
                MyApplication.ShowMassage(MessageFragment.this,
                        "Please connect to working Internet connection!");
                return;
            } else {

                final ProgressDialog dialog = ProgressDialog.show(MessageFragment.this, "", "Fetching Data....");

                System.out.println("deviceid" + MyApplication.getDeviceID());
                new GetDataTask(MessageFragment.this, "notification.php?device_id=" + MyApplication.getDeviceID(),
                        new CompleteListener() {

                            @Override
                            public void onRemoteErrorOccur(Object error) {
                                dialog.dismiss();
                                // actionBarFragment.mHandler
                                // .removeCallbacks(actionBarFragment.mRunnable);

                                getMessageData();

                            }

                            @Override
                            public void onRemoteCallComplete(Object result) {

                                System.out.println("Response::" + result);
                                dialog.dismiss();

                                try {
                                    JSONObject object = new JSONObject(result.toString());

                                    JSONArray array = object.getJSONArray("data");
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject inObject = array.getJSONObject(i);
                                        LinkMessageWrapper wrapper = new LinkMessageWrapper();
                                        wrapper.setMessageId(i);
                                        wrapper.setMessage(inObject.getString("notification"));
                                        wrapper.setMessageDate(inObject.getString("createdon"));
                                        wrapper.setDate_to_order((double) getMillisTime(inObject.getString("createdon"), "yyyy-MM-dd HH:mm:ss"));
                                        wrapper.setMessageUrl("");
                                        messageList.add(wrapper);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                getMessageData();

                            }
                        }).execute();

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public long getMillisTime(String time, String inputPattern) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        try {
            Date date = inputFormat.parse(time);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
