package com.mymdsmanager.datacontrollers;

public class Constants {
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static int MEDICINE_TYPE = -1;
    public static final int PRESCRIPTION = 1, OVER_THE_COUNTER = 2, SUUPLEMENTS = 3;
    public static final String INSURANCE_PRIMARY = "p";
    public static final String INSURANCE_SECONDRY = "s";
    public static final int MEDICAL_PROFESSIONAL = 1, SYMPTOM_TRACKER = 4, MEDICINE = 2, NOTES = 3;
    public static int searched_type = 0;
    //    http://mds.devsiteurl.com/mds_tracker/api/diagnosis_test.php
    //   public static final String APP_URL = "http://www.mds-foundation.org/mds_manager/api/";
    public static final String APP_URL = "http://mds-manager.devsiteurl.com/api/";
//    public static final String APP_URL = "http://www.mds-foundation.org/mds_manager4/api/";
    public static final String additional_resourceUrl = "http://www.mds-foundation.org/patient-caregiver-resources-api/";
    public static final String clinicalUrl = "http://www.mds-foundation.org/mds_manager/api/clinical-trial-api/";
    public static boolean add_reminder = true;
    public static String PERSON_NAME = "personname";
    public static String PERSON_BIRTHDAY = "bday";
    public static String PERSON_PROFILEIMAGE = "p_image";
    public static String PERSONE_GENDER = "p_gender";
    public static String UPLOAD_DATA_KEY = "upload_data_key";
    public static String USER_UPLOAD_DATA_KEY = "user_upload_data_key";
    public static String genderStr;


    public static final String MOLECULAR_URL = "https://www.mds-foundation.org/building-blocks-of-hope/quicktip/Molecular";
    public static final String IPSS_URL = "https://www.mds-foundation.org/building-blocks-of-hope/quicktip/IPSSR";
    public static String IS_MOLECULAR_FIRST_TIME = "is_molecular_first_time";
    public static String IS_IPSS_R_FIRST_TIME = "is_ipss_s_first_time";
    public static String IS_SYMPTON_TRACKER_FIRST_TIME = "is_symtom_tracker_first_time";

    public static String IS_OVERWRITE_SERVER = "is_overwriteserver";

    public static final int MY_PERMISSION_CAMERA = 55;
    public static final int PERMISSION_READ_EXTERNAL_STORAGE = 56;
    public static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 57;

}
