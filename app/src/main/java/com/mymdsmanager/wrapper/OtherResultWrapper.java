package com.mymdsmanager.wrapper;

/**
 * Created by suarebits on 1/7/16.
 */
public class OtherResultWrapper {
    String otherrowid = "", blabid = "", labtitle = "", labvalue;
    String date = "";
    Double date_to_order;

    public String getOtherrowid() {
        return otherrowid;
    }

    public void setOtherrowid(String otherrowid) {
        this.otherrowid = otherrowid;
    }

    public String getBlabid() {
        return blabid;
    }

    public void setBlabid(String blabid) {
        this.blabid = blabid;
    }

    public String getLabtitle() {
        return labtitle;
    }

    public void setLabtitle(String labtitle) {
        this.labtitle = labtitle;
    }

    public String getLabvalue() {
        return labvalue;
    }

    public void setLabvalue(String labvalue) {
        this.labvalue = labvalue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getDate_to_order() {
        return date_to_order;
    }

    public void setDate_to_order(Double date_to_order) {
        this.date_to_order = date_to_order;
    }
}
