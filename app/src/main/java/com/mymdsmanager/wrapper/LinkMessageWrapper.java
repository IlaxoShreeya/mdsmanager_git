package com.mymdsmanager.wrapper;

/**
 * Created by suarebits on 29/3/17.
 */
public class LinkMessageWrapper {

    public String message = "";
    public String messageDate = "";

    public Double getDate_to_order() {
        return date_to_order;
    }

    public void setDate_to_order(Double date_to_order) {
        this.date_to_order = date_to_order;
    }

    private Double date_to_order;
    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public int messageId;

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String messageUrl = "";
}
