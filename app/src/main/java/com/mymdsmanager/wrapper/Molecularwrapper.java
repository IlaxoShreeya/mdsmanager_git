package com.mymdsmanager.wrapper;

/**
 * Created by suarebits on 29/3/17.
 */
public class Molecularwrapper {


    public String idh2Value = "";
    public String idh1Value = "";
    public String tet2Value = "";
    public String tp53Value = "";
    public String sf3b1Value = "";
    public String srsf2Value = "";
    public String jak2Value = "";
    public String asxl1Value = "";
    public String dnmt3aValue = "";
    public String ezh2Value = "";
    public String etv6Value = "";
    public String runx1Value = "";
    public String u2af1Value = "";
    public String zrsr2Value = "";
    public String nraskrasValue = "";

    public String getCblValue() {
        return cblValue;
    }

    public void setCblValue(String cblValue) {
        this.cblValue = cblValue;
    }

    public String getNraskrasValue() {
        return nraskrasValue;
    }

    public void setNraskrasValue(String nraskrasValue) {
        this.nraskrasValue = nraskrasValue;
    }

    public String getZrsr2Value() {
        return zrsr2Value;
    }

    public void setZrsr2Value(String zrsr2Value) {
        this.zrsr2Value = zrsr2Value;
    }

    public String getU2af1Value() {
        return u2af1Value;
    }

    public void setU2af1Value(String u2af1Value) {
        this.u2af1Value = u2af1Value;
    }

    public String getRunx1Value() {
        return runx1Value;
    }

    public void setRunx1Value(String runx1Value) {
        this.runx1Value = runx1Value;
    }

    public String getEtv6Value() {
        return etv6Value;
    }

    public void setEtv6Value(String etv6Value) {
        this.etv6Value = etv6Value;
    }

    public String getEzh2Value() {
        return ezh2Value;
    }

    public void setEzh2Value(String ezh2Value) {
        this.ezh2Value = ezh2Value;
    }

    public String getDnmt3aValue() {
        return dnmt3aValue;
    }

    public void setDnmt3aValue(String dnmt3aValue) {
        this.dnmt3aValue = dnmt3aValue;
    }

    public String getAsxl1Value() {
        return asxl1Value;
    }

    public void setAsxl1Value(String asxl1Value) {
        this.asxl1Value = asxl1Value;
    }

    public String getJak2Value() {
        return jak2Value;
    }

    public void setJak2Value(String jak2Value) {
        this.jak2Value = jak2Value;
    }

    public String getSrsf2Value() {
        return srsf2Value;
    }

    public void setSrsf2Value(String srsf2Value) {
        this.srsf2Value = srsf2Value;
    }

    public String getSf3b1Value() {
        return sf3b1Value;
    }

    public void setSf3b1Value(String sf3b1Value) {
        this.sf3b1Value = sf3b1Value;
    }

    public String getTp53Value() {
        return tp53Value;
    }

    public void setTp53Value(String tp53Value) {
        this.tp53Value = tp53Value;
    }

    public String getTet2Value() {
        return tet2Value;
    }

    public void setTet2Value(String tet2Value) {
        this.tet2Value = tet2Value;
    }

    public String getIdh1Value() {
        return idh1Value;
    }

    public void setIdh1Value(String idh1Value) {
        this.idh1Value = idh1Value;
    }

    public String getIdh2Value() {
        return idh2Value;
    }

    public void setIdh2Value(String idh2Value) {
        this.idh2Value = idh2Value;
    }

    public String cblValue = "";
}



