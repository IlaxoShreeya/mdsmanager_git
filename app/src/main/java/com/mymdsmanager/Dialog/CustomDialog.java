package com.mymdsmanager.Dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by NAVIN on 7/29/2017.
 */

public class CustomDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    public interface InterfaceCommunicator {
        void sendRequestCode(int Code);
    }

    String activityCode;
    Button btn;
    public CustomDialog(){
        super();
    }

    @SuppressLint("ValidFragment")
    public CustomDialog(String activityCode, Button btn){
        super();
        this.activityCode = activityCode;
        this.btn = btn;
    }
    InterfaceCommunicator interfaceCommunicator;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.getDatePicker().setMaxDate(new Date().getTime());
        return dialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        btn.setText(String.valueOf(month + "/" + day + "/" + year));
        interfaceCommunicator.sendRequestCode(1);
    }

    @Override
    public void onAttach(Activity activity) {
        interfaceCommunicator = (InterfaceCommunicator) activity;
        super.onAttach(activity);
    }
    @Override
    public void onDetach() {
        interfaceCommunicator = null;
        super.onDetach();
    }
}
